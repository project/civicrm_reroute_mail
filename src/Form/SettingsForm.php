<?php

namespace Drupal\civicrm_reroute_email\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures CiviCRM Reroute Email settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Drupal\Component\Utility\EmailValidatorInterface definition.
   *
   * @var \Drupal\Component\Utility\EmailValidatorInterface
   */
  protected $emailValidator;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->emailValidator = $container->get('email.validator');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'civicrm_reroute_email_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['civicrm_reroute_email.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('civicrm_reroute_email.settings');

    $form['enable'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable CiviCRM rerouting'),
      '#default_value' => $config->get('enable'),
      '#description' => $this->t('Check this box if you want to enable CiviCRM email rerouting. Uncheck to disable rerouting.'),
    ];

    $form['email_address'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Email address'),
      '#default_value' => $config->get('email_address'),
      '#description' => $this->t('Provide an email address to reroute all emails to this address.'),
      '#states' => [
        'visible' => [
          ':input[name="enable"]' => [
            'checked' => TRUE,
          ],
        ],
        'required' => [
          ':input[name="enable"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!empty($form_state->getValue('email_address')) && !$this->emailValidator->isValid($form_state->getValue('email_address'))) {
      $form_state->setErrorByName('email_address', $this->t('Invalid email address.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('civicrm_reroute_email.settings')
      ->set('enable', $values['enable'])
      ->set('email_address', $values['email_address'])
      ->save();

    parent::submitForm($form, $form_state);
  }

}
